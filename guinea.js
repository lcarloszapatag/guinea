

$(function () { //Listo el documento
    AOS.init();

    $('.navbar-nav .nav-item a').click(function (e) {
        /*   e.preventDefault(); */
        // $(".navbar-nav .nav-item.active").css({borderBottom:'0px solid black'});
        $(".navbar-nav .nav-item a.active").removeClass("active"); //Remove any "active" class  
        $(this).addClass("active"); //Add "active" class to selected tab 
        // $(this).css({borderBottom:'3px solid orange'});
    });

  


    $('.opciones_imagenes').click(function () {
        imagen = $(this).attr('src');
        $('.opciones_imagenes').removeClass('seleccionada');
        $(this).addClass('seleccionada');

        if (imagen != $('#imgActual').attr('src')) {
            $('#imgActual').fadeOut(400, 'linear', function () {
                $('#imgActual').attr("src", imagen);
            })
            $('#imgActual').fadeIn(400);
        }
    })

    $('#mapaGuinea').hover(function () {
        $(this).addClass('transition');
    }, function () {
        $(this).removeClass('transition');
    });


    /* Funciona a medias */
    // $(document).on("scroll", onScroll);

    // function onScroll(event){
    //   var scrollPos = $(document).scrollTop();
    //   $('.nav-item a').each(function () {
    //       var currLink = $(this);
    //       var refElement = $(currLink.attr("href"));
    //       if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
    //           $('.nav-item a').removeClass("active");
    //           currLink.addClass("active");
    //       }
        
    //       });
    //   }


});

